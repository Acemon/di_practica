package obj;

/**
 * @author Fernando Gallego
 * @version 1.0
 * @since 25/02/2016
 * recommended java version: 1.8 or later
 */
public class Backup {
    private String nombre;
    private long peso;

    /**
     * Constructor sin inicializar
     */
    public Backup() {
    }

    @Override
    public String toString() {
        return nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public long getPeso() {
        return peso;
    }

    public void setPeso(long peso) {
        this.peso = peso;
    }
}
