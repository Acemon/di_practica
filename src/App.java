import base.Controller;
import base.Model;
import base.Ventana;

import javax.swing.JFileChooser;

/**
 * @author Fernando Gallego
 * @version 1.0 
 * @since 10/02/2016
 * recommended java version: 1.8 or later
 */
public class App {
	/**
	 * Inicio de aplicacion
	 * @param args
	 */
	public static void main (String[] args){
		Ventana view = new Ventana();
		Model model = new Model();
		new Controller(model, view);
	}
}
