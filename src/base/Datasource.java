package base;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import obj.Backup;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Fernando Gallego
 * @version 1.0
 * @since 25/02/2016
 * recommended java version: 1.8 or later
 */
public class Datasource implements JRDataSource{
    List<Backup> backups = new ArrayList<>();
    int contador =-1;

    /**
     * En el caso de que existan mas valores en el vector, pasara a la siguiente posicion del mismo
     * @return si existe otro valor
     * @throws JRException
     */
    @Override
    public boolean next() throws JRException {
        return ++contador<backups.size();
    }

    /**
     * Este metodo es obligatorio, nos rellena el .jasper
     * @param jrField
     * @return  Devuelve el valor que se necesite el el reporte
     * @throws JRException
     */
    @Override
    public Object getFieldValue(JRField jrField) throws JRException {
        Object value = null;
        if (jrField.getName().equalsIgnoreCase("nombre")){
            value = backups.get(contador).getNombre();
        }else if (jrField.getName().equalsIgnoreCase("peso")){
            value = backups.get(contador).getPeso();
        }
        return value;
    }

    /**
     * Añadimos un objeto al vector
     * @param backup objeto
     */
    public void addbackup(Backup backup){
        backups.add(backup);
    }
}
