package base;
/**
 * @author Fernando Gallego
 * @version 1.0 
 * @since 10/02/2016
 * recommended java version: 1.8 or later
 */
import javax.swing.*;

import org.jfree.chart.ChartPanel;
import org.swixml.SwingEngine;

import java.awt.*;

public class Ventana{
	JTable tabla;
	JPanel panelpcp;
	ChartPanel cpanel;
	JScrollPane jsp;
	JPanel panelscs;
	JPanel panelbt;
	JButton btQuitar;
	JButton btAnadir;
	JComboBox cbTablas;
	JLabel lcopias;
	JLabel lfecha;
    JComboBox cbHora;
    JComboBox cbMinuto;
    JButton btFecha;
	
	JMenuItem conectar;
	JMenuItem informe;
	JMenuItem quit;
	JMenuItem copia;
	JMenuItem cargar;
	JMenuItem ruta;

	/**
	 * Constructor de la ventana, leer xml, asignar JButtons, JLabels... a JPanels
	 */
	public Ventana(){
		try {
			new SwingEngine(this).render("xml.xml").setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		tabla = new JTable();
		jsp = new JScrollPane(tabla);
		panelbt = new JPanel(new GridLayout(2,4));
		lcopias = new JLabel("Copia diaria");
		btQuitar = new JButton("Quitar");
		cbTablas = new JComboBox();
		btAnadir = new JButton("Añadir");
        lfecha = new JLabel("Hora");
        cbHora = new JComboBox();
        cbMinuto =  new JComboBox();
        btFecha = new JButton("ajustar hora");
		panelbt.add(lcopias);
		panelbt.add(btQuitar);
        panelbt.add(cbTablas);
        panelbt.add(btAnadir);
        panelbt.add(lfecha);
        panelbt.add(cbHora);
        panelbt.add(cbMinuto);
        panelbt.add(btFecha);
        rellenarcb();
        panelscs = new JPanel(new BorderLayout());
        panelscs.add(panelbt, BorderLayout.SOUTH);
        panelscs.add(jsp, BorderLayout.CENTER);
		panelpcp.add(panelscs, BorderLayout.WEST);
		cpanel = new ChartPanel(null);
		panelpcp.add(cpanel, BorderLayout.CENTER);
		
	}

	/**
	 * Rellena los JComboBox para que el usuario pueda elegir la hora y minuto de cuando hacer las copias
	 * de seguridad automaticas
	 */
    private void rellenarcb() {
	    for (int i =0; i<60; i++)
	        cbMinuto.addItem(i);
	    for (int i=0; i<24; i++)
	        cbHora.addItem(i);
    }
}
