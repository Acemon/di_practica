package base;
/**
 * @author Fernando Gallego
 * @version 1.0 
 * @since 10/02/2016
 * recommended java version: 1.8 or later
 */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.sql.SQLException;
import java.util.Calendar;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class Controller implements ActionListener, ListSelectionListener{
	private Ventana view;
	private Model model;
	private ListSelectionModel lsm;
	private File backup;
	private String IP;
	private String USUARIO;
	private String PSW;
	private String ruta;
	private int fecha;
	private String nombreBBDD;
	int hora;
	int minuto;

    /**
     * Constructor del Controller.
     * @param model Clase model que opera con la gestion de datos.
     * @param view Clase de la ventana.
     */
	public Controller(Model model, Ventana view) {
		this.model = model;
		this.view = view;
		this.fecha = 7;
		nombreBBDD = null;
		hora = 19;
		minuto = 30;
		ruta = System.getProperty("user.home");
		iniciarHilo();
		listenerAndCommands();
	}

    /**
     * Aqui manejo todos los botones que tengo tanto en el menu item como los
     * botones de la ventana.
     * @param arg0 ActionEvent
     */
	@Override
	public void actionPerformed(ActionEvent arg0) {
		switch (arg0.getActionCommand()) {
            case "conectar":
                JFrame frame = new JFrame("Conectar");
                Conectar conectar = new Conectar(frame);
                conectar.showDialog();
                if (!conectar.ok)
                    break;
                IP = conectar.tfIp.getText();
                USUARIO = conectar.tfUsuario.getText();
                PSW = new String(conectar.tfPswd.getPassword());
                conectar.dispose();
                try {
                    model.conectarBBDD(IP, USUARIO, PSW);
                    view.tabla.setModel(model.obtenerBBDDs());
                    view.panelpcp.updateUI();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;

            case "informe":
                model.generarInforme(ruta);
                break;

            case "quit":
                System.exit(0);
                break;

            case "copia":
                try {
                    obtenerFecha(nombreBBDD);
                    model.hacerBackup(nombreBBDD,USUARIO, PSW, backup);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;

            case "cargar":
                try {
                    JFileChooser jfc = new JFileChooser();
                    jfc.showOpenDialog(jfc);
                    File pathFile = jfc.getSelectedFile();
                    model.cargarBackup(USUARIO, PSW, pathFile);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;

            case "ruta":
                JFileChooser jfc = new JFileChooser();
                jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                jfc.showOpenDialog(jfc);
                ruta = jfc.getCurrentDirectory().toString();
                System.out.println(ruta);
                break;

            case "anadir":
                if (nombreBBDD == null)
                    break;
                view.cbTablas.addItem(nombreBBDD);
                break;

            case "quitar":
                if (view.cbTablas.getSelectedItem() == null)
                    break;
                view.cbTablas.removeItem(view.cbTablas.getSelectedItem());
                break;

            case "fecha":
                hora = (int) view.cbHora.getSelectedItem();
                minuto = (int) view.cbMinuto.getSelectedItem();
                System.out.println(view.cbHora.getSelectedIndex());
                break;
		}
        view.cbHora.setSelectedItem(hora);
        view.cbMinuto.setSelectedItem(minuto);
	}

    /**
     * Creo el nombre que llevaran los archivos del backup de la base de datos
     * @param bbdd nombre de la base de datos para ponerla en el file
     */
	private void obtenerFecha(String bbdd) {
		Calendar c = Calendar.getInstance();
		StringBuilder fecha = new StringBuilder();
		fecha.append("_");
		fecha.append(bbdd);
		fecha.append("_");
		fecha.append(String.valueOf(c.get(Calendar.YEAR)));
		fecha.append("_");
		fecha.append(String.valueOf(c.get(Calendar.MONTH)));
		fecha.append("_");
		fecha.append(String.valueOf(c.get(Calendar.DATE)));
		fecha.append("_");
		fecha.append(String.valueOf(c.get(Calendar.HOUR_OF_DAY)));
		fecha.append("-");
		fecha.append(String.valueOf(c.get(Calendar.MINUTE)));
		fecha.append("-");
		fecha.append(String.valueOf(c.get(Calendar.SECOND)));
		backup = new File(ruta+File.separator+fecha.toString()+".sql");
        System.out.println(backup);
    }

    /**
     * Pongo todos los listener y actionCommands con los que tengo que trabajar
     */
    private void listenerAndCommands() {
		view.conectar.addActionListener(this);
		view.informe.addActionListener(this);
		view.quit.addActionListener(this);
		view.copia.addActionListener(this);
		view.ruta.addActionListener(this);
		view.cargar.addActionListener(this);
		view.tabla.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		lsm = view.tabla.getSelectionModel();
		lsm.addListSelectionListener(this);
		view.btAnadir.addActionListener(this);
		view.btQuitar.addActionListener(this);
		view.btFecha.addActionListener(this);
		
		view.cargar.setActionCommand("cargar");
		view.conectar.setActionCommand("conectar");
		view.informe.setActionCommand("informe");
		view.quit.setActionCommand("quit");
		view.copia.setActionCommand("copia");
		view.ruta.setActionCommand("ruta");
        view.btAnadir.setActionCommand("anadir");
        view.btQuitar.setActionCommand("quitar");
        view.btFecha.setActionCommand("fecha");
		
	}

    /**
     * Exclusivamente para saber que dato ha seleccionado de la JTable
     * @param e ListSelectionEvent
     */
	@Override
	public void valueChanged(ListSelectionEvent e) {
		nombreBBDD = (String) view.tabla.getValueAt(view.tabla.getSelectedRow(), 0);
		try {
			view.cpanel.setChart(model.obtenerDatos(nombreBBDD));
			view.cpanel.updateUI();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
	}

    /**
     * Inicia el hilo que llevara las copias de seguridad diarias
     */
	private void iniciarHilo() {
		Thread hilo = new Thread(new Runnable() {
			@Override
			public void run() {
				while(true){
                    Calendar c = Calendar.getInstance();
				    if (hora == c.get(Calendar.HOUR_OF_DAY)){
                        System.out.println(minuto +" "+ c.get(Calendar.MINUTE));
                        if (minuto == c.get(Calendar.MINUTE)){
				            for (int i =0; i<=view.cbTablas.getItemCount()-1; i++){
                                System.out.println("dentro");
                                String copia = (String) view.cbTablas.getItemAt(i);
				                obtenerFecha(copia);
                                try {
                                    model.hacerBackup(copia, USUARIO, PSW, backup);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                    try {
                        Thread.sleep(60000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
				}

			}
		});
		hilo.start();
	}
}
