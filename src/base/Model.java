package base;
/**
 * @author Fernando Gallego
 * @version 1.0 
 * @since 10/02/2016
 * recommended java version: 1.8 or later
 */
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;
import java.util.stream.Stream;

import javax.swing.table.DefaultTableModel;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import obj.Backup;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;

public class Model {
	
	private Connection con;

	/**
	 * Conecta con la base de datos
	 * @param IP ip de la base de datos
	 * @param USUARIO nombre del usuario
	 * @param PSWD Contraseña para acceder
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public void conectarBBDD(String IP, String USUARIO, String PSWD) throws SQLException, ClassNotFoundException{
		Class.forName("com.mysql.jdbc.Driver");
		con = DriverManager.getConnection("jdbc:mysql://"+IP+":3306",USUARIO,PSWD);
	}

	/**
	 * Obtiene las bases de datos que residen en mysql
	 * @return devuelve un dtm listo para poner en la JTable
	 * @throws SQLException Posible error de SQL
	 */
	public DefaultTableModel obtenerBBDDs() throws SQLException{
		ResultSet rs = con.getMetaData().getCatalogs();
		DefaultTableModel dtm = new DefaultTableModel(new String[]{"BBDD"}, 0);
		while (rs.next()){
			Vector <String> tab = new Vector<>();
			tab.add(rs.getString(1));
			dtm.addRow(tab);
		}
		return dtm;
	}

	/**
	 * Obtiene el peso de cada una de las tablas de la base seleccionada
	 * @param bbdd Base de datos
	 * @return JFreeChart listo para poner en un grafico tipo chart
	 * @throws SQLException Posible error de SQL
	 */
	public JFreeChart obtenerDatos(String bbdd) throws SQLException{
		String sentenciasql = "SELECT TABLE_SCHEMA, TABLE_NAME, "
				+ "ROUND((DATA_LENGTH + INDEX_LENGTH) / 1024) AS `Size (KB)` "
				+ "FROM  information_schema.TABLES "
				+ "WHERE  TABLE_SCHEMA = ? "
				+ "ORDER BY  (DATA_LENGTH + INDEX_LENGTH)DESC";
		
		PreparedStatement sentencia = con.prepareStatement(sentenciasql);
		sentencia.setString(1, bbdd);
		ResultSet rs = sentencia.executeQuery();
		DefaultPieDataset dpd = new DefaultPieDataset();
		while (rs.next()){
			dpd.setValue(rs.getString(2), rs.getInt(3));
		}
		JFreeChart jfc = ChartFactory.createPieChart(bbdd, dpd, false, false, false);
		return jfc;
	}

	/**
	 * Realiza un backup de la base de datos de tipo .sql
	 * @param nombreBBDD nombre de la base de datos a buscar
	 * @param backup Archivo y ruta donde se creara el backup
	 * @throws IOException Posible error de que no pueda crear el archivo .sql
	 */
	public void hacerBackup(String nombreBBDD, String USUARIO, String PWD, File backup) throws IOException {
		Runtime runtime=Runtime.getRuntime();
		FileWriter fw=new FileWriter(backup);
		Process child=runtime.exec("C:\\xampp\\mysql\\bin\\mysqldump --opt --password="+PWD+" --user="+USUARIO+" --databases "+nombreBBDD+" -R");
		InputStreamReader irs=new InputStreamReader(child.getInputStream());
		BufferedReader br=new BufferedReader(irs);

		String line;
		while((line=br.readLine()) !=null){
			fw.write(line +"\n");
		}
		fw.close();
		irs.close();
		br.close();
	}

	/**
	 * Realiza la carga del archivo .sql hacia la base de datos
	 * @param USUARIO usuario de la bbdd
	 * @param PSW contraseña de la bbdd
	 * @param pathFile ruta donde se encuentra el archivo
	 * @throws IOException Posible error al buscar el archivo
	 */
	public void cargarBackup(String USUARIO, String PSW, File pathFile) throws IOException {
		String[] executeCmd = new String[]{"C:\\xampp\\mysql\\bin\\mysql", "--user=" + USUARIO, "--password=" + PSW, "-e", "source " + pathFile};
		Runtime runtime=Runtime.getRuntime();
		runtime.exec(executeCmd);
	}

	/**
	 * Genera el informe y lo muestra en un jasperviewer
	 * @param ruta
	 */
	public void generarInforme(String ruta){
		Datasource data = new Datasource();
		try(Stream<Path> paths = Files.walk(Paths.get(ruta))) {
			paths.forEach(filePath -> {
				if (Files.isRegularFile(filePath)) {
					System.out.println(filePath);
					Backup backup = new Backup();
					backup.setNombre(filePath.getFileName().toString());
					backup.setPeso(filePath.toFile().length());
					data.addbackup(backup);
				}
			});
			JasperReport jasper = (JasperReport) JRLoader.loadObjectFromFile("reportePractica.jasper");
			JasperPrint printer = JasperFillManager.fillReport(jasper, null, data);
			JasperViewer.viewReport(printer);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JRException e) {
			e.printStackTrace();
		}
		System.out.println("Generar Informe");
	}
}
