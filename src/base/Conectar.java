package base;
/**
 * @author Fernando Gallego
 * @version 1.0 
 * @since 10/02/2016
 * recommended java version: 1.8 or later
 */
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class Conectar extends JDialog implements ActionListener{
	private static final long serialVersionUID = 1L;
	private JPanel panelbase;
	private JLabel lIp;
	JTextField tfIp;
	private JLabel lPswd;
	JPasswordField tfPswd;
	private JLabel lUsuario;
	JTextField tfUsuario;
	private JButton btOk;
	private JButton btCancelar;
	Boolean ok;

	/**
	 * Constructor de la clase ventana
	 * @param frame frame bajo el que se monta la ventana
	 */
	public Conectar(JFrame frame) {
		super(frame, true);
		panelbase= new JPanel(new GridLayout(4,2));
		setLocationRelativeTo(null);
		setContentPane(panelbase);
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		lIp = new JLabel("IP");
		tfIp = new JTextField();
		lPswd = new JLabel("Contraseña");
		tfPswd = new JPasswordField();
		lUsuario = new JLabel("Usuario");
		tfUsuario = new JTextField();
		btOk = new JButton("Aceptar");
		btCancelar = new JButton("Cancelar");
		panelbase.add(lIp);
		panelbase.add(tfIp);
		panelbase.add(lUsuario);
		panelbase.add(tfUsuario);
		panelbase.add(lPswd);
		panelbase.add(tfPswd);
		panelbase.add(btOk);
		panelbase.add(btCancelar);
		ok = false;
		listenersyActioncm();
		pack();
		setModal(true);
	}

	/**
	 * listeners y action commands para trabajar con estos botones
	 */
	private void listenersyActioncm() {
		btCancelar.addActionListener(this);
		btOk.addActionListener(this);
		btCancelar.setActionCommand("Cancelar");
		btOk.setActionCommand("Ok");
	}


	@Override
	/**
	 * Me permite saber si el usuario quiere continuar o cancelar el loguin
	 */
	public void actionPerformed(ActionEvent e) {
		String m = e.getActionCommand();
		if (m.equalsIgnoreCase("Ok")){
			ok = true;
		}else if (m.equalsIgnoreCase("Cancelar")){
			ok = false;
		}
		setVisible(false);
	}

	/**
	 * Con este metodo, puedo decicir cuando mostrar la ventana desde la clase controller
	 */
	public void showDialog() {
		setVisible(true);
	}
}
